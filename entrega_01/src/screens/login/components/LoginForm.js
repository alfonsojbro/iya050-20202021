import React, { Component } from "react";

import Button from "@material-ui/core/Button";

import { Wrapper, InputWrapper, ButtonWrapper } from "./styles";
import Input from "../../../common/CustomInput";
import UserContext from "../../../providers/UserContext";
import loginUser from "../../../database/loginUser";
import getUserDetails from "../../../database/getUserDetails";
class LoginForm extends Component {
  static contextType = UserContext;

  state = {
    username: "",
    password: "",
  };
  _usernameInputRef;

  componentDidMount() {
    this._usernameInputRef.focus();
  }

  onClickEnterButton = () => {
    this.handleLogin();
  };

  handleLogin = () => {
    const { username, password } = this.state;
    const { setSnackbarError, setSnackbarMessage } = this.props;
    const { setUser, user } = this.context;
    loginUser(username, password)
      .then((response) => {
        const { sessionToken } = response;

        {
          sessionToken
            ? getUserDetails(sessionToken).then((userDetails) => {
                const { firstName, username } = userDetails;
                setSnackbarMessage("Welcome to Desmoche sir");
                setUser({
                  fullName: firstName,
                  username,
                  loggedIn: true,
                  token: sessionToken,
                });
              })
            : setSnackbarError(
                "There was a problem signing in. Please try again."
              );
        }
      })
      .catch((error) => {
        console.log(error);
        var errorMessage = error.message;
        setSnackbarError(errorMessage);
      });
  };

  render() {
    const { username, password } = this.state;

    return (
      <Wrapper>
        <InputWrapper>
          <Input
            inputRef={(input) => {
              this._usernameInputRef = input;
            }}
            onChange={(event) =>
              this.setState({ username: event.target.value })
            }
            value={username}
            label="Usuario"
            placeholder=""
            id="username"
            type="text"
            error=""
          />
        </InputWrapper>
        <Input
          onChange={(event) => this.setState({ password: event.target.value })}
          value={password}
          type="password"
          placeholder=""
          label="Contraseña"
          id="password"
          error=""
        />
        <ButtonWrapper>
          <Button
            disabled={password.length < 6}
            variant="outlined"
            onClick={this.onClickEnterButton}
            color="primary"
          >
            Iniciar Sesión
          </Button>
        </ButtonWrapper>
      </Wrapper>
    );
  }
}

export default LoginForm;
