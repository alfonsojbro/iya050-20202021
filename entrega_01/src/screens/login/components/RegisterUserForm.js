import React, { Component } from "react";

import Button from "@material-ui/core/Button";

import { Wrapper, InputWrapper, ButtonWrapper } from "./styles";
import Input from "../../../common/CustomInput";
import UserContext from "../../../providers/UserContext";
import createUser from "../../../database/createUser";
import getUserDetails from "../../../database/getUserDetails";
import loginUser from "../../../database/loginUser";
class RegisterUserForm extends Component {
  static contextType = UserContext;
  state = {
    repeatedPassword: "",
    password: "",
    username: "",
    fullName: "",
  };

  onClickRegisterButton = () => {
    const { repeatedPassword, password, username, fullName } = this.state;

    const { setSnackbarMessage, setSnackbarError } = this.props;

    const isSamePassword = password === repeatedPassword;

    if (!isSamePassword) {
      setSnackbarError("The passwords are not the same");
      return;
    }

    if (password.length < 6) {
      setSnackbarError("The password must have at least 6 characters");
      return;
    }

    createUser(fullName, username, password)
      .then((userRegistered) => {
        const { setUser } = this.context;
        const { username } = userRegistered;

        {
          username
            ? loginUser(username, password)
                .then((response) => {
                  const { sessionToken } = response;

                  {
                    sessionToken
                      ? getUserDetails(sessionToken).then((userDetails) => {
                          const { firstName, username } = userDetails;
                          if (firstName) {
                            setSnackbarMessage("Welcome to Desmoche sir");
                            setUser({
                              fullName: firstName,
                              username,
                              loggedIn: true,
                              token: sessionToken,
                            });
                          } else {
                            setSnackbarError(
                              "There was a problem retrieving your information. Please try signing in."
                            );
                          }
                        })
                      : setSnackbarError(
                          "There was a problem signing in. Please try signing in."
                        );
                  }
                })
                .catch((e) =>
                  setSnackbarError(
                    "There was an error signing in. Please check your credentials."
                  )
                )
            : setSnackbarError(
                "There was a error signing up. Please try again."
              );
        }
      })
      .catch((e) =>
        setSnackbarError(
          "There was an error signing up. Please try with a different user."
        )
      );
  };

  renderForm = () => {
    const { fullName, username, password, repeatedPassword } = this.state;
    return (
      <div>
        <InputWrapper>
          <Input
            onChange={(event) =>
              this.setState({ fullName: event.target.value })
            }
            onBlur={() => {}}
            placeholder=""
            value={fullName}
            label="Nombre y Apellido"
            type="text"
            id="name"
            error=""
          />
        </InputWrapper>
        <InputWrapper>
          <Input
            onChange={(event) =>
              this.setState({ username: event.target.value })
            }
            onBlur={() => {}}
            placeholder=""
            value={username}
            label="Nombre de usuario"
            type="text"
            id="name"
            error=""
          />
        </InputWrapper>
        <InputWrapper>
          <Input
            onChange={(event) =>
              this.setState({ password: event.target.value })
            }
            placeholder=""
            value={password}
            label="Contraseña"
            type="password"
            id="name"
            error=""
          />
        </InputWrapper>
        <InputWrapper>
          <Input
            onChange={(event) =>
              this.setState({ repeatedPassword: event.target.value })
            }
            placeholder=""
            value={repeatedPassword}
            label="Repetir Contraseña"
            type="password"
            id="name"
            error=""
          />
        </InputWrapper>
        <ButtonWrapper>
          <Button
            id="signUp"
            onClick={this.onClickRegisterButton}
            variant="outlined"
            color="primary"
          >
            Crear Cuenta
          </Button>
        </ButtonWrapper>
      </div>
    );
  };

  render() {
    return <Wrapper>{this.renderForm()}</Wrapper>;
  }
}

export default RegisterUserForm;
