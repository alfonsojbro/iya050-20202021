import React, { Component } from "react";

import Paper from "@material-ui/core/Paper";

import styled from "styled-components";
import Snackbar from "../../common/Snackbar";
import LoginForm from "./components/LoginForm";
import RegisterUserForm from "./components/RegisterUserForm";

import UserContext from "../../providers/UserContext";
const Container = styled.div`
  flex: 1;
  height: 100vh;
  width: 100%;

  background-repeat: no-repeat;
  background-position: center top;
  background-attachment: fixed;
  background-size: 100%;

  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const LogoWrapper = styled.div`
  width: 200px;
  height: 200px;
  display: flex;
  justify-content: center;

  align-items: center;
  padding: 24px;
  border-radius: 36px;
`;

const LogoLetter = styled.img`
  width: 200px;
  height: 200px;
  margin-bottom: 42px;
  color: white;
`;

const ContentContainer = styled(({ ...props }) => <Paper {...props} />)`
  padding: 24px;
`;

const ChangeTabButton = styled.p`
  margin-top: 16px;
  font-size: 16px;
  font-weight: 500;
  white-space: pre-wrap;
  color: grey;
`;

class Login extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      isSnackbarOpen: false,
      snackbarMessage: "",
      snackbarError: "",
      isLoginOpen: true,
    };
  }

  onToggleSnackbar = () => {
    const { isSnackbarOpen } = this.state;

    this.setState({
      isSnackbarOpen: !isSnackbarOpen,
    });
  };

  setSnackbarMessage = (message) => {
    this.setState({
      isSnackbarOpen: true,
      snackbarMessage: message,
      snackbarError: "",
    });
  };

  setSnackbarError = (error) => {
    this.setState({
      isSnackbarOpen: true,
      snackbarError: error,
      snackbarMessage: "",
    });
  };

  createNewUser = (id, fullName, email) => {
    const newUser = {
      id,
      fullName,
      email,
    };
    setNewUser(newUser);
    //this.props.logUser(newUser);
  };

  renderRegisterUserForm = () => (
    <RegisterUserForm
      setSnackbarMessage={this.setSnackbarMessage}
      setSnackbarError={this.setSnackbarError}
      createUser={this.createUser}
    />
  );

  renderLoginForm = () => (
    <LoginForm
      setSnackbarError={this.setSnackbarError}
      setSnackbarMessage={this.setSnackbarMessage}
    />
  );

  changeTab = () => {
    const { isLoginOpen } = this.state;
    this.setState({ isLoginOpen: !isLoginOpen });
  };
  render() {
    const {
      snackbarMessage,
      isSnackbarOpen,
      snackbarError,
      isLoginOpen,
    } = this.state;

    const changeTabMessage = isLoginOpen
      ? "Registrate aquí"
      : "¿Ya tienes cuenta? Pulsa acá";

    return (
      <Container>
        <Wrapper>
          <ContentContainer>
            {this.state.isLoginOpen
              ? this.renderLoginForm()
              : this.renderRegisterUserForm()}
            <ChangeTabButton onClick={this.changeTab}>
              {changeTabMessage}
            </ChangeTabButton>
          </ContentContainer>

          <Wrapper>
            <Snackbar
              onCloseSnackbar={this.onToggleSnackbar}
              message={snackbarMessage}
              isOpen={isSnackbarOpen}
              error={snackbarError}
            />
          </Wrapper>
        </Wrapper>
      </Container>
    );
  }
}

export default Login;
