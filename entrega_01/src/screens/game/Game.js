import React, { useContext } from "react";

import AppBar from "@material-ui/core/AppBar";

import Toolbar from "../../common/Toolbar";

const Game = () => {
  return (
    <AppBar style={{ position: "fixed" }} position="static" color="secondary">
      <Toolbar />
    </AppBar>
  );
};

export default Game;
