import React, { useContext } from "react";

import Toolbar from "@material-ui/core/Toolbar";

import styled from "styled-components";
import UserContext from "../providers/UserContext";
import UserInfo from "./UserInfo";
const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Title = styled.p`
  margin-left: 16px;
  font-size: 20px;
  font-weight: 500;
  color: blue;
`;

const LeftSideContainer = styled.div`
  display: flex;
  align-items: center;
`;

const RightSideContainer = styled.div`
  display: flex;
  align-items: center;
`;

const HeaderBar = () => {
  return (
    <Toolbar>
      <Container>
        <LeftSideContainer></LeftSideContainer>
        <RightSideContainer>
          <UserInfo></UserInfo>
        </RightSideContainer>
      </Container>
    </Toolbar>
  );
};

export default HeaderBar;
