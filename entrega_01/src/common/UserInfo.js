import React, { Component, Fragment } from "react";

import AssignmentInd from "@material-ui/icons/AssignmentInd";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";

import styled from "styled-components";
import UserContext from "../providers/UserContext";
import signOutUser from "../database/signOutUser";

const UsernameText = styled.p`
  color: white;
`;

const UserIcon = styled(AssignmentInd)`
  margin-right: 8px;
  color: white;
`;

class UserInfo extends Component {
  static contextType = UserContext;
  state = {
    anchorElement: null,
  };

  onClickButton = (event) => {
    this.setState({ anchorElement: event.currentTarget });
  };

  handleLogout = () => {
    const { user, setUser } = this.context;

    signOutUser(user.token);
    setUser({ ...user, loggedIn: false, token: "" });
  };

  handleCloseMenu = () => {
    this.setState({ anchorElement: null });
  };

  render() {
    const { anchorElement } = this.state;
    const { user } = this.context;

    return (
      <Fragment>
        <Button
          aria-owns={anchorElement ? "user-menu" : undefined}
          onClick={this.onClickButton}
          aria-haspopup="true"
        >
          <UserIcon />
          <UsernameText>{user ? `${user.fullName}` : "Usuario"}</UsernameText>
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorElement}
          open={Boolean(anchorElement)}
          onClose={this.handleCloseMenu}
        >
          <MenuItem onClick={this.handleLogout}>Cerrar Sesión</MenuItem>
        </Menu>
      </Fragment>
    );
  }
}

export default UserInfo;
