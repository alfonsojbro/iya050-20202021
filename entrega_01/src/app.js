import { hot } from "react-hot-loader/root";

import React, { Fragment, useContext } from "react";

import Root from "./Root";
import { UserProvider } from "./providers/UserContext";

const App = () => {
  return (
    <UserProvider>
      <Root />
    </UserProvider>
  );
};

export default hot(App);
