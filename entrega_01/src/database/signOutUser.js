import CONSTANTS from "../resources/CONSTANTS";

export default (token) => {
  return fetch(`${CONSTANTS.LOGIN_API_URL}/sessions/${token}`, {
    method: "DELETE",
    headers: new Headers({ Authorization: token }),
  }).then((res) => {
    return res.json();
  });
};
