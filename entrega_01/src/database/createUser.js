import CONSTANTS from "../resources/CONSTANTS";

export default (fullName, username, password) => {
  return fetch(`${CONSTANTS.LOGIN_API_URL}/users/${username}`, {
    method: "POST",
    body: JSON.stringify({
      firstName: fullName,
      password: password,
    }),
  }).then((res) => {
    return res.json();
  });
};
