import CONSTANTS from "../resources/CONSTANTS";

export default (token) => {
  return fetch(`${CONSTANTS.LOGIN_API_URL}/user/`, {
    method: "GET",
    headers: { Authorization: token },
  }).then((res) => {
    return res.json();
  });
};
