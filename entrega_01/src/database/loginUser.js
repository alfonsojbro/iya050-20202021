import CONSTANTS from "../resources/CONSTANTS";

export default (username, password) => {
  return fetch(`${CONSTANTS.LOGIN_API_URL}/users/${username}/sessions`, {
    method: "POST",
    body: JSON.stringify({
      password,
    }),
  }).then((res) => {
    return res.json();
  });
};
