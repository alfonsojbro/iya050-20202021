import { hot } from "react-hot-loader/root";

import { BrowserRouter as ApplicationRouter, Switch } from "react-router-dom";

import React, { Fragment, useContext } from "react";
import Login from "./screens/login/Login";
import Game from "./screens/game/Game";
import UserProvider from ".//providers/UserContext";

const Root = () => {
  const { user } = useContext(UserProvider);
  console.log(user);
  return (
    <ApplicationRouter>
      <Fragment>
        {user.loggedIn ? (
          <Switch>
            <Game />
          </Switch>
        ) : (
          <Login />
        )}
      </Fragment>
    </ApplicationRouter>
  );
};

export default hot(Root);
