import { expect } from "chai";
import { Card, CARD_VALUE, SUIT } from "../types/Card";
import { Deck } from "../types/Deck";
import { joinRoom, createNewGame, Game } from "../types/Game";
import { Meld, isValid, updateGameWMeld } from "../types/Meld";
import { passTurnNextPlayer, TURN_TYPE } from "../types/Turn";

describe("Desmoche online", () => {
  describe("createNewGame", () => {
    describe("given an invalid playerId", () => {
      it("should return null game", () => {
        const playerId = "";
        const wins = 0;
        const loses = 0;

        const game = createNewGame({ playerId, wins, loses });
        expect(game).equal(null);
      });
    });

    describe("given correct input params", () => {
      it("should return a valid game", () => {
        const playerId = "arrrggg";
        const wins = 0;
        const loses = 0;

        const game = createNewGame({ playerId, wins, loses });

        const expectedTurn = {
          holderPlayerId: playerId,
          playerId,
          type: TURN_TYPE.DRAW,
        };

        expect(game.deck.cards.length).equal(43);
        expect(game.players[0].deck.cards.length).equal(9);

        expect(game.turn).to.deep.equal(expectedTurn);
      });
    });
  });

  describe("Join a Room", () => {
    describe("given an invalid roomId", () => {
      it("should return null", () => {
        const roomId: string | null = null;
        const playerId: string | null = null;
        const wins = 0;
        const loses = 0;

        const roomJoined = joinRoom({ roomId, playerId, wins, loses });

        expect(null).equal(roomJoined);
      });
    });
    describe("given valid params", () => {
      it("should return valid deck combination", () => {
        const roomId = "123456";
        const playerId = "987645321";
        const wins = 23;
        const loses = 34;

        const gameJoined = joinRoom({ roomId, playerId, wins, loses });

        const lastPlayer = gameJoined.players[gameJoined.players.length - 1];

        expect(lastPlayer.deck.cards.length).equal(9);
        expect(gameJoined.deck.cards.length).equal(34);
      });
    });
  });

  describe("Make a meld", () => {
    describe("given empty cards quantity", () => {
      it("should return false", () => {
        const cards: Card[] = [];

        const meld = isValid(cards);

        expect(false).equal(meld);
      });
    });
    describe("given an invalid card combination", () => {
      it("should return false", () => {
        const cards = [
          { suit: SUIT.EPEE, value: CARD_VALUE.CINQ },
          { suit: SUIT.COEUR, value: CARD_VALUE.SIX },
          { suit: SUIT.DIAMANT, value: CARD_VALUE.SEPT },
        ];

        const meld = isValid(cards);

        expect(false).equal(meld);
      });
    });

    describe("given a straight", () => {
      it("should return true", () => {
        const cards = [
          { suit: SUIT.COEUR, value: CARD_VALUE.CINQ },
          { suit: SUIT.COEUR, value: CARD_VALUE.SIX },
          { suit: SUIT.COEUR, value: CARD_VALUE.SEPT },
        ];

        const meld = isValid(cards);

        expect(true).equal(meld);
      });
    });

    describe("given same number", () => {
      it("should return true", () => {
        const cards = [
          { suit: SUIT.COEUR, value: CARD_VALUE.CINQ },
          { suit: SUIT.DIAMANT, value: CARD_VALUE.CINQ },
          { suit: SUIT.TREFLE, value: CARD_VALUE.CINQ },
        ];

        const meld = isValid(cards);

        expect(true).equal(meld);
      });
    });

    describe("given a valid meld", () => {
      it("should return a updated game with the meld", () => {
        const cards = [
          { suit: SUIT.COEUR, value: CARD_VALUE.CINQ },
          { suit: SUIT.DIAMANT, value: CARD_VALUE.CINQ },
          { suit: SUIT.TREFLE, value: CARD_VALUE.CINQ },
        ];
        const melds: Meld[] = [];
        const deck: Deck = { cards: [] };
        const player = {
          playerId: "Ahoy",
          wins: 0,
          loses: 0,
          melds,
        };
        const game: Game = {
          roomId: "",
          createdAt: "",
          deck,
          turn: {
            playerId: "",
            holderPlayerId: "",
            type: TURN_TYPE.DRAW,
          },
          players: [player],
        };

        const gameUpdated = updateGameWMeld(game, player, { cards });

        expect(gameUpdated.players[0].melds[0].cards).to.deep.equal(cards);
      });
    });
  });

  describe("Player drops a card", () => {
    describe("Given correct params", () => {
      it("should pass the PICK turn to the next player", () => {
        const players = [
          { playerId: "Ahoy", wins: 20, loses: 30 },
          { playerId: "Ahoy2", wins: 30, loses: 10 },
        ];
        const turn = {
          playerId: "Ahoy",
          type: TURN_TYPE.DRAW,
          holderPlayerId: "Ahoy",
        };

        const expectedTurn = {
          playerId: "Ahoy2",
          type: TURN_TYPE.PICK,
          holderPlayerId: "Ahoy",
        };
        const actualTurn = passTurnNextPlayer(players, turn);

        expect(expectedTurn).to.deep.equal(actualTurn);
      });

      it("should pass the DRAW turn to the next player", () => {
        const players = [
          { playerId: "Ahoy", wins: 20, loses: 30 },
          { playerId: "Ahoy2", wins: 30, loses: 10 },
        ];
        const turn = {
          playerId: "Ahoy2",
          type: TURN_TYPE.PICK,
          holderPlayerId: "Ahoy",
        };

        const expectedTurn = {
          playerId: "Ahoy2",
          type: TURN_TYPE.DRAW,
          holderPlayerId: "Ahoy2",
        };
        const actualTurn = passTurnNextPlayer(players, turn);

        console.log(actualTurn);
        expect(expectedTurn).to.deep.equal(actualTurn);
      });
    });
  });
});
