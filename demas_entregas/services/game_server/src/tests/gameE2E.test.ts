import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import gameMock from "./mocks/game.mock.json";
import invalidGame from "./mocks/gameInvalid.mock.json";
import dropCard from "./mocks/dropCard.mock.json";
import dropCardInvalid from "./mocks/dropCardInvalid.mock.json";
chai.use(chaiHttp);
const url = "http://localhost:3000";

describe("Desmoche online E2E", () => {
  /* describe("Create a New Game", () => {
    describe("given a valid playerId", () => {
      it("should return success to false game", (done) => {
        chai
          .request(url)
          .get("/game?playerId=20")
          .end(function (err, res) {
            expect(res.body).to.have.property("success").to.be.equal(true);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
    describe("given a invalid playerId", () => {
      it("should return success to false", (done) => {
        chai
          .request(url)
          .get("/game?playerId=")
          .end(function (err, res) {
            expect(res.body).to.have.property("success").to.be.equal(false);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
  });

  describe("Join a room", () => {
    describe("given a valid room Id and player Id", () => {
      it("should return success to true game", (done) => {
        chai
          .request(url)
          .get("/joinRoom?roomId=fdasf&playerId=adfdas")
          .end(function (err, res) {
            expect(res.body).to.have.property("success").to.be.equal(true);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
    describe("given a invalid room Id", () => {
      it("should return success to false game", (done) => {
        chai
          .request(url)
          .get("/joinRoom?roomId=&playerId=34543")
          .end(function (err, res) {
            expect(res.body).to.have.property("success").to.be.equal(false);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
    describe("given a invalid player Id", () => {
      it("should return success to false game", (done) => {
        chai
          .request(url)
          .get("/joinRoom?roomId=arg&playerId=")
          .end(function (err, res) {
            expect(res.body).to.have.property("success").to.be.equal(false);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
  });
*/
  describe("Make a Meld", () => {
    describe("given a valid game, playerId, and melds", () => {
      it("should return success to true ", (done) => {
        chai
          .request(url)
          .post("/makeMeld")
          .send(gameMock)
          .end(function (err, res) {
            console.log(res.body);
            expect(res.body).to.have.property("success").to.be.equal(true);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
    describe("given null params", () => {
      it("should return success to false", (done) => {
        chai
          .request(url)
          .post("/makeMeld")
          .send(invalidGame)
          .end(function (err, res) {
            console.log(res.body);
            expect(res.body).to.have.property("success").to.be.equal(false);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
  });

  describe("Drop a card", () => {
    describe("given valid params", () => {
      it("should return success to true ", (done) => {
        chai
          .request(url)
          .post("/dropCard")
          .send(dropCard)
          .end(function (err, res) {
            console.log(res.body);
            expect(res.body).to.have.property("success").to.be.equal(true);
            expect(res).to.have.status(200);
            done();
          });
      });
    });

    describe("given invalid params", () => {
      it("should return success to false ", (done) => {
        chai
          .request(url)
          .post("/dropCard")
          .send(dropCardInvalid)
          .end(function (err, res) {
            console.log(res.body);
            expect(res.body).to.have.property("success").to.be.equal(false);
            expect(res).to.have.status(200);
            done();
          });
      });
    });
  });
});
