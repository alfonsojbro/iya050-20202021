import Express from "express";
import bodyParser from "body-parser";

import { createNewGame, Game, generateRoomCode, joinRoom } from "./types/Game";

import { createFreshDeck, Deck, removeCardFromPlayer } from "./types/Deck";
import { Player } from "./types/Player";
import { passTurnNextPlayer, Turn, TURN_TYPE } from "./types/Turn";
import { isValid, Meld, updateGameWMeld } from "./types/Meld";
import { Card } from "./types/Card";
const serverURL =
  "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";
const app: any = Express();
const port = 3000;
app.use(Express.json());
app.use(Express.urlencoded({ extended: true }));
app.use(Express.json());

/* A player makes a meld and updates the deck
 Here it notifies if someone wins or loses the game */
app.post(
  "/makeMeld",
  (req: { body: any }, res: { send: (arg0: any) => void }) => {
    const { game, meld }: { game: Game; meld: Meld } = req.body;
    // Validate is a valid combination (Straight | SameNumber)

    if (!game || !meld) {
      res.send({ success: false, game: null });
      return;
    }
    /* if (!isValid(meld.cards)) {
      res.send({ message: "Invalid Combination", succes: true, game });
      return;
    }
*/
    const gameUpdated = updateGameWMeld(game, meld);

    gameUpdated.turn = { ...gameUpdated.turn, type: TURN_TYPE.DROP };

    console.log(gameUpdated.players[0]);
    const success = gameUpdated ? true : false;
    res.send({ success, game: gameUpdated });
  }
);

/*Player throws card to waste pile*/
app.post("/dropCard", (req: { body: any }, res: { send: any }) => {
  const { game, card }: { game: Game; card: Card } = req.body;

  console.log("Drop req", req.body);
  if (!game || !card) {
    res.send({ success: false, game: null });
    return;
  }
  game.players = removeCardFromPlayer(game.players, card, game.turn.playerId);
  game.turn = passTurnNextPlayer(game.players, game.turn);

  game.waste = card;

  const success = game.turn ? true : false;
  res.send({ success, game });
});
/*Player passes his turn*/
app.post("/passTurn", (req: { body: any }, res: { send: any }) => {
  const { game }: { game: Game } = req.body;

  if (!game) {
    res.send({ success: false, game: null });
    return;
  }
  game.turn = passTurnNextPlayer(game.players, game.turn);

  const success = game.turn ? true : false;
  res.send({ success, game });
});
/*Player draws a card to waste pile*/
app.post("/drawCard", (req: { body: any }, res: { send: any }) => {
  const { game }: { game: Game } = req.body;
  console.log(req.body);
  if (!game) {
    res.send({ success: false, game: null });
    return;
  }
  const popCard = game.deck.cards.pop();

  game.waste = popCard;
  game.turn.type = TURN_TYPE.PICK;
  const success = game.turn ? true : false;
  res.send({ success, game });
});
/* Update the stats of the players involved in the game once one wins or loses*/
app.post(
  "/gameOver/:game&:playerId",

  (
    req: { query: { game: Game; meld: Meld; playerId: string } },
    res: { sendStatus: (arg0: number) => void }
  ) => {
    const { game, playerId }: { game: Game; meld: Meld; playerId: string } =
      req.query;

    game.players.map((player) => {
      if (playerId === player.playerId) {
        player.wins = player.wins++;
      } else {
        player.loses = player.loses++;
      }
      return player;
    });
    //Update on persistance database the score of the players
    //notifyDatabse(game.players)

    res.sendStatus(200);
  }
);

app.listen(port, () => console.log("Listening on port " + port));
