import { Player } from "./Player";

export type Turn = {
  playerId: string;
  type: TURN_TYPE;
  holderPlayerId: string;
};

export enum TURN_TYPE {
  DRAW = "DRAW",
  DROP = "DROP",
  PICK = "PICK",
}

export const passTurnNextPlayer = (players: Player[], turn: Turn): Turn => {
  const currentPlayerIndex = players.findIndex(
    (player) => turn.playerId === player.playerId
  );

  if (currentPlayerIndex === undefined) return null;
  let nextPlayerIndex = 0;

  //Not the last player
  if (currentPlayerIndex !== players.length - 1) {
    nextPlayerIndex = currentPlayerIndex + 1;
  }
  let nextPlayer = players[nextPlayerIndex];

  let turnType: TURN_TYPE = TURN_TYPE.PICK;

  let currentHolderPlayerId = turn.holderPlayerId;
  let nextPlayerId = nextPlayer.playerId;
  if (nextPlayer.playerId === currentHolderPlayerId) {
    //SKIP THE NEXT PLAYER AND PASS THE TURN TO THE PLAYER AFTER THAT TO DRAW A CARD
    turnType = TURN_TYPE.DRAW;
    //Not the last player
    if (nextPlayerIndex != players.length - 1) {
      const nextNextPlayerIndex = nextPlayerIndex + 1;
      nextPlayer = players[nextNextPlayerIndex];
      currentHolderPlayerId = players[nextNextPlayerIndex].playerId;
      nextPlayerId = players[nextNextPlayerIndex].playerId;
    } else {
      nextPlayer = players[0];
      currentHolderPlayerId = players[0].playerId;
      nextPlayerId = players[0].playerId;
    }
  }
  //Pass the turn to the next player
  return {
    holderPlayerId: currentHolderPlayerId,
    type: turnType,
    playerId: nextPlayerId,
  };
};
