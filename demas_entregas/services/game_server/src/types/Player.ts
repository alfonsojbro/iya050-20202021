import { Deck } from "./Deck";

import { Meld } from "./Meld";

export type Player = {
  playerId: string;
  wins: number;
  loses: number;
  deck?: Deck;
  melds?: Meld[];
};
