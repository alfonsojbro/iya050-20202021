import { Card, CARD_VALUE, SUIT, getCard } from "./Card";
import { Player } from "./Player";

export type Deck = {
  cards: Card[];
};

export const createFreshDeck = (): Card[] => {
  //@ts-ignore
  return shuffle(
    //@ts-ignore
    Object.keys(SUIT).flatMap((suitKey) =>
      Object.keys(CARD_VALUE).map((cardKey) =>
        //@ts-ignore
        getCard(SUIT[suitKey], CARD_VALUE[cardKey])
      )
    )
  );
};

export const shuffle = (deck: Card[]) => {
  for (let i = deck.length - 1; i > 0; i--) {
    const newIndex = Math.floor(Math.random() * (i + 1));
    const oldValue = deck[newIndex];
    deck[newIndex] = deck[i];
    deck[i] = oldValue;
  }
  return deck;
};

export const removeCardFromPlayer = (
  players: Player[],
  card: Card,
  playerId: string
) => {
  players.map((player) => {
    if (player.playerId === playerId) {
      return player.deck.cards.map((_card, index) => {
        if (card.suit === _card.suit && card.value === _card.value) {
          player.deck.cards.splice(index, 1);
        }
      });
    }
  });
  return players;
};
/*const newPlayers =  players.map((player) => {
    if (player.playerId === playerId) {
     return player.deck.cards.filter((_card) => {
        _card.suit === card.suit && _card.value === card.value
      });
    }
  });
  return newPlayers;*/
