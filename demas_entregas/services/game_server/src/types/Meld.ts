import { Card } from "./Card";
import { Game } from "./Game";
import { Player } from "./Player";

export type Meld = {
  cards: Card[];
};

export function isValid(cards: Card[]): boolean {
  if (cards.length < 3) return false;
  const cardsSelectedSorted = cards.sort((a, b) => {
    const aValue = convertFromStringToNumberValue(a.value);
    const bValue = convertFromStringToNumberValue(b.value);
    return parseInt(aValue) - parseInt(bValue);
  });
  if (isStraight(cardsSelectedSorted) || isSameNumber(cardsSelectedSorted)) {
    return true;
  }

  return false;
}
function isStraight(cards: Card[]) {
  let previousCard = cards[0];
  for (let x = 1; x < cards.length; x++) {
    const value = (parseInt(previousCard.value, 10) + 1).toString();
    const suit = previousCard.suit;
    if (value != cards[x].value || suit != cards[x].suit) {
      return false;
    }
    previousCard = cards[x];
  }
  return true;
}
function isSameNumber(cards: Card[]) {
  let value = cards[0].value;
  for (let x = 0; x < cards.length; x++) {
    if (cards[x].value !== value) {
      return false;
    }
  }
  return true;
}

function convertFromStringToNumberValue(value: string) {
  if (value === "J") value = "11";
  else if (value === "Q") value = "12";
  else if (value === "K") value = "13";
  else if (value === "A") value = "1";

  return value;
}

export function updateGameWMeld(game: Game, meld: Meld) {
  //Update player melds with new meld
  const player = game.players.find(
    (_player) => _player.playerId === game.turn.playerId
  );
  if (!player.melds) player.melds = [];
  player.melds.push(meld);

  console.log("PlayerCards", player.deck.cards);
  console.log("MeldCards", meld.cards);

  player.deck.cards = player.deck.cards.filter((_card) => {
    return (
      meld.cards.filter(
        (_meldCard) =>
          _meldCard.suit === _card.suit && _meldCard.value === _card.value
      ).length == 0
    );
  });
  const playerIndex = game.players.findIndex(
    (gamePlayer) => gamePlayer.playerId === player.playerId
  );
  game.players[playerIndex] = player;
  return game;
}
