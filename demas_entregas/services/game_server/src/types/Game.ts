import { create } from "node:domain";
import { Card } from "./Card";
import { createFreshDeck, Deck } from "./Deck";
import { Player } from "./Player";
import { Turn, TURN_TYPE } from "./Turn";

export type Game = {
  roomId: string;
  createdAt: string;
  players: Player[];
  waste?: Card;
  deck: Deck;
  turn: Turn;
};

export const generateRoomCode = () => {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};

export const createNewGame = ({
  playerId,
  wins,
  loses,
}: {
  playerId: string;
  wins: number;
  loses: number;
}) => {
  if (!playerId) return null;
  const deckCards = createFreshDeck();
  const roomId = generateRoomCode();
  const deckSplitPoint = deckCards.length - 9;

  const mainDeck: Deck = { cards: deckCards.slice(0, deckSplitPoint) };
  const playerDeck: Deck = {
    cards: deckCards.slice(deckSplitPoint, deckCards.length),
  };
  const players: Player[] = [];
  const playerCreator: Player = {
    playerId,
    wins,
    loses,
    deck: playerDeck,
    melds: [],
  };
  players.push(playerCreator);

  const game: Game = {
    roomId,
    turn: {
      holderPlayerId: playerId,
      type: TURN_TYPE.DRAW,
      playerId,
    },
    players,
    createdAt: new Date().toDateString(),
    deck: mainDeck,
    waste: null,
  };
  return game;
};

export const joinRoom = ({
  roomId,
  playerId,
  wins,
  loses,
}: {
  roomId: string;
  playerId: string;
  wins: number;
  loses: number;
}) => {
  if (!roomId || !playerId) return null;

  //TODO: Get the room from the persistance service by roomId
  //Creating a dummy game for now
  const game = createNewGame({ playerId, wins: 20, loses: 30 });

  const deckSplitPoint = game.deck.cards.length - 9;
  const playerDeck: Deck = {
    cards: game.deck.cards.slice(deckSplitPoint, game.deck.cards.length),
  };
  const mainDeck = { cards: game.deck.cards.slice(0, deckSplitPoint) };

  const player: Player = {
    playerId,
    wins,
    loses,
    deck: playerDeck,
    melds: [],
  };
  game.players.push(player);
  game.deck = mainDeck;
  return game;
};
