export type Card = {
  value: CARD_VALUE;
  suit: SUIT;
};

export const getCard = (suit: SUIT, value: CARD_VALUE): Card => {
  return {
    suit,
    value,
  };
};
export enum SUIT {
  EPEE = "♠",
  TREFLE = "♣",
  COEUR = "♥",
  DIAMANT = "♦",
}
export enum CARD_VALUE {
  A = "A",
  DEUX = "2",
  TROIS = "3",
  QUATRE = "4",
  CINQ = "5",
  SIX = "6",
  SEPT = "7",
  HUIT = "8",
  NEUF = "9",
  DIX = "10",
  ONZE = "J",
  DOUZE = "Q",
  TREIZE = "K",
}
