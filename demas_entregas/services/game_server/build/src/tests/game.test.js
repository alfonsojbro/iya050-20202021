"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Card_1 = require("../types/Card");
const Game_1 = require("../types/Game");
const Meld_1 = require("../types/Meld");
const Turn_1 = require("../types/Turn");
describe("Desmoche online", () => {
    describe("createNewGame", () => {
        describe("given an invalid playerId", () => {
            it("should return null game", () => {
                const playerId = "";
                const wins = 0;
                const loses = 0;
                const game = Game_1.createNewGame({ playerId, wins, loses });
                chai_1.expect(game).equal(null);
            });
        });
        describe("given correct input params", () => {
            it("should return a valid game", () => {
                const playerId = "arrrggg";
                const wins = 0;
                const loses = 0;
                const game = Game_1.createNewGame({ playerId, wins, loses });
                const expectedTurn = {
                    holderPlayerId: playerId,
                    playerId,
                    type: Turn_1.TURN_TYPE.DRAW,
                };
                chai_1.expect(game.deck.cards.length).equal(43);
                chai_1.expect(game.players[0].deck.cards.length).equal(9);
                chai_1.expect(game.turn).to.deep.equal(expectedTurn);
            });
        });
    });
    describe("Join a Room", () => {
        describe("given an invalid roomId", () => {
            it("should return null", () => {
                const roomId = null;
                const playerId = null;
                const wins = 0;
                const loses = 0;
                const roomJoined = Game_1.joinRoom({ roomId, playerId, wins, loses });
                chai_1.expect(null).equal(roomJoined);
            });
        });
        describe("given valid params", () => {
            it("should return valid deck combination", () => {
                const roomId = "123456";
                const playerId = "987645321";
                const wins = 23;
                const loses = 34;
                const gameJoined = Game_1.joinRoom({ roomId, playerId, wins, loses });
                const lastPlayer = gameJoined.players[gameJoined.players.length - 1];
                chai_1.expect(lastPlayer.deck.cards.length).equal(9);
                chai_1.expect(gameJoined.deck.cards.length).equal(34);
            });
        });
    });
    describe("Make a meld", () => {
        describe("given empty cards quantity", () => {
            it("should return false", () => {
                const cards = [];
                const meld = Meld_1.isValid(cards);
                chai_1.expect(false).equal(meld);
            });
        });
        describe("given an invalid card combination", () => {
            it("should return false", () => {
                const cards = [
                    { suit: Card_1.SUIT.EPEE, value: Card_1.CARD_VALUE.CINQ },
                    { suit: Card_1.SUIT.COEUR, value: Card_1.CARD_VALUE.SIX },
                    { suit: Card_1.SUIT.DIAMANT, value: Card_1.CARD_VALUE.SEPT },
                ];
                const meld = Meld_1.isValid(cards);
                chai_1.expect(false).equal(meld);
            });
        });
        describe("given a straight", () => {
            it("should return true", () => {
                const cards = [
                    { suit: Card_1.SUIT.COEUR, value: Card_1.CARD_VALUE.CINQ },
                    { suit: Card_1.SUIT.COEUR, value: Card_1.CARD_VALUE.SIX },
                    { suit: Card_1.SUIT.COEUR, value: Card_1.CARD_VALUE.SEPT },
                ];
                const meld = Meld_1.isValid(cards);
                chai_1.expect(true).equal(meld);
            });
        });
        describe("given same number", () => {
            it("should return true", () => {
                const cards = [
                    { suit: Card_1.SUIT.COEUR, value: Card_1.CARD_VALUE.CINQ },
                    { suit: Card_1.SUIT.DIAMANT, value: Card_1.CARD_VALUE.CINQ },
                    { suit: Card_1.SUIT.TREFLE, value: Card_1.CARD_VALUE.CINQ },
                ];
                const meld = Meld_1.isValid(cards);
                chai_1.expect(true).equal(meld);
            });
        });
        describe("given a valid meld", () => {
            it("should return a updated game with the meld", () => {
                const cards = [
                    { suit: Card_1.SUIT.COEUR, value: Card_1.CARD_VALUE.CINQ },
                    { suit: Card_1.SUIT.DIAMANT, value: Card_1.CARD_VALUE.CINQ },
                    { suit: Card_1.SUIT.TREFLE, value: Card_1.CARD_VALUE.CINQ },
                ];
                const melds = [];
                const deck = { cards: [] };
                const player = {
                    playerId: "Ahoy",
                    wins: 0,
                    loses: 0,
                    melds,
                };
                const game = {
                    roomId: "",
                    createdAt: "",
                    deck,
                    turn: {
                        playerId: "",
                        holderPlayerId: "",
                        type: Turn_1.TURN_TYPE.DRAW,
                    },
                    players: [player],
                };
                const gameUpdated = Meld_1.updateGameWMeld(game, player, { cards });
                chai_1.expect(gameUpdated.players[0].melds[0].cards).to.deep.equal(cards);
            });
        });
    });
    describe("Player drops a card", () => {
        describe("Given correct params", () => {
            it("should pass the PICK turn to the next player", () => {
                const players = [
                    { playerId: "Ahoy", wins: 20, loses: 30 },
                    { playerId: "Ahoy2", wins: 30, loses: 10 },
                ];
                const turn = {
                    playerId: "Ahoy",
                    type: Turn_1.TURN_TYPE.DRAW,
                    holderPlayerId: "Ahoy",
                };
                const expectedTurn = {
                    playerId: "Ahoy2",
                    type: Turn_1.TURN_TYPE.PICK,
                    holderPlayerId: "Ahoy",
                };
                const actualTurn = Turn_1.passTurnNextPlayer(players, turn);
                chai_1.expect(expectedTurn).to.deep.equal(actualTurn);
            });
            it("should pass the DRAW turn to the next player", () => {
                const players = [
                    { playerId: "Ahoy", wins: 20, loses: 30 },
                    { playerId: "Ahoy2", wins: 30, loses: 10 },
                ];
                const turn = {
                    playerId: "Ahoy2",
                    type: Turn_1.TURN_TYPE.PICK,
                    holderPlayerId: "Ahoy",
                };
                const expectedTurn = {
                    playerId: "Ahoy2",
                    type: Turn_1.TURN_TYPE.DRAW,
                    holderPlayerId: "Ahoy2",
                };
                const actualTurn = Turn_1.passTurnNextPlayer(players, turn);
                console.log(actualTurn);
                chai_1.expect(expectedTurn).to.deep.equal(actualTurn);
            });
        });
    });
});
