"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = __importStar(require("chai"));
const chai_http_1 = __importDefault(require("chai-http"));
const game_mock_json_1 = __importDefault(require("./mocks/game.mock.json"));
const gameInvalid_mock_json_1 = __importDefault(require("./mocks/gameInvalid.mock.json"));
const dropCard_mock_json_1 = __importDefault(require("./mocks/dropCard.mock.json"));
const dropCardInvalid_mock_json_1 = __importDefault(require("./mocks/dropCardInvalid.mock.json"));
chai_1.default.use(chai_http_1.default);
const url = "http://localhost:3000";
describe("Desmoche online E2E", () => {
    describe("Create a New Game", () => {
        describe("given a valid playerId", () => {
            it("should return success to false game", (done) => {
                chai_1.default
                    .request(url)
                    .get("/game?playerId=20&wins=10&loses=15")
                    .end(function (err, res) {
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(true);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
        describe("given a invalid playerId", () => {
            it("should return success to false", (done) => {
                chai_1.default
                    .request(url)
                    .get("/game?playerId=&wins=10&loses=15")
                    .end(function (err, res) {
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(false);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
    });
    describe("Join a room", () => {
        describe("given a valid room Id and player Id", () => {
            it("should return success to true game", (done) => {
                chai_1.default
                    .request(url)
                    .get("/joinRoom?roomId=fdasf&playerId=adfdas&loses=60&wins=100")
                    .end(function (err, res) {
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(true);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
        describe("given a invalid room Id", () => {
            it("should return success to false game", (done) => {
                chai_1.default
                    .request(url)
                    .get("/joinRoom?roomId=&playerId=34543&loses=15&wins=20")
                    .end(function (err, res) {
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(false);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
        describe("given a invalid player Id", () => {
            it("should return success to false game", (done) => {
                chai_1.default
                    .request(url)
                    .get("/joinRoom?roomId=arg&playerId=&loses=&wins=")
                    .end(function (err, res) {
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(false);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
    });
    describe("Make a Meld", () => {
        describe("given a valid game, playerId, and melds", () => {
            it("should return success to true ", (done) => {
                chai_1.default
                    .request(url)
                    .post("/makeMeld")
                    .send(game_mock_json_1.default)
                    .end(function (err, res) {
                    console.log(res.body);
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(true);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
        describe("given null params", () => {
            it("should return success to false", (done) => {
                chai_1.default
                    .request(url)
                    .post("/makeMeld")
                    .send(gameInvalid_mock_json_1.default)
                    .end(function (err, res) {
                    console.log(res.body);
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(false);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
    });
    describe("Drop a card", () => {
        describe("given valid params", () => {
            it("should return success to true ", (done) => {
                chai_1.default
                    .request(url)
                    .post("/dropCard")
                    .send(dropCard_mock_json_1.default)
                    .end(function (err, res) {
                    console.log(res.body);
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(true);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
        describe("given invalid params", () => {
            it("should return success to false ", (done) => {
                chai_1.default
                    .request(url)
                    .post("/dropCard")
                    .send(dropCardInvalid_mock_json_1.default)
                    .end(function (err, res) {
                    console.log(res.body);
                    chai_1.expect(res.body).to.have.property("success").to.be.equal(false);
                    chai_1.expect(res).to.have.status(200);
                    done();
                });
            });
        });
    });
});
