"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Game_1 = require("./types/Game");
const Turn_1 = require("./types/Turn");
const Meld_1 = require("./types/Meld");
const app = express_1.default();
const port = 3000;
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
//Get player info
app.get("getPlayerInfo", (req, res) => {
    const { playerId } = req.query;
    //Get the player by id from the persistance service
    const player = {
        playerId,
        wins: Math.random(),
        loses: Math.random(),
    };
    res.send(player);
});
//Create a new game for players to join with a single player
app.get("/game", (req, res) => {
    console.log(req.query);
    const { playerId, wins, loses } = req.query;
    const game = Game_1.createNewGame({ playerId, wins, loses });
    const success = game ? true : false;
    //Save the game on the persistance service
    console.log(game);
    res.send({ success, game: game });
});
//Join the room
app.get("/joinRoom", (req, res) => {
    const { roomId, playerId, wins, loses } = req.query;
    const gameJoined = Game_1.joinRoom({ roomId, playerId, wins, loses });
    const success = gameJoined ? true : false;
    //
    console.log(gameJoined);
    res.send({ success, game: gameJoined });
});
/* A player makes a meld and updates the deck
 Here it notifies if someone wins or loses the game */
app.post("/makeMeld", (req, res) => {
    const { game, meld, playerId, } = req.body;
    // Validate is a valid combination (Straight | SameNumber)
    if (!game || !meld || !playerId) {
        res.send({ success: false, game: null });
        return;
    }
    if (!Meld_1.isValid(meld.cards)) {
        res.send({ message: "Invalid Combination", succes: true, game });
        return;
    }
    const player = game.players.find((player) => player.playerId === playerId);
    if (!player) {
        res.send({
            message: "Not player found with that Id",
            succes: false,
            game: null,
        });
        return;
    }
    const gameUpdated = Meld_1.updateGameWMeld(game, player, meld);
    gameUpdated.turn = Turn_1.passTurnNextPlayer(gameUpdated.players, gameUpdated.turn);
    const success = gameUpdated ? true : false;
    res.send({ success, game: gameUpdated });
});
/*Player throws card to waste pile*/
app.post("/dropCard", (req, res) => {
    const { game, card } = req.body;
    if (!game || !card) {
        res.send({ success: false, game: null });
        return;
    }
    game.turn = Turn_1.passTurnNextPlayer(game.players, game.turn);
    game.waste = card;
    const success = game.turn ? true : false;
    res.send({ success, game });
});
/* Update the stats of the players involved in the game once one wins or loses*/
app.post("/gameOver/:game&:playerId", (req, res) => {
    const { game, playerId, } = req.query;
    game.players.map((player) => {
        if (playerId === player.playerId) {
            player.wins = player.wins++;
        }
        else {
            player.loses = player.loses++;
        }
        return player;
    });
    //Update on persistance database the score of the players
    //notifyDatabse(game.players)
    res.sendStatus(200);
});
app.listen(port, () => console.log("Listening on port " + port));
