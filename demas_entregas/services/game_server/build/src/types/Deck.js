"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.shuffle = exports.createFreshDeck = void 0;
const Card_1 = require("./Card");
const createFreshDeck = () => {
    //@ts-ignore
    return exports.shuffle(
    //@ts-ignore
    Object.keys(Card_1.SUIT).flatMap((suitKey) => Object.keys(Card_1.CARD_VALUE).map((cardKey) => 
    //@ts-ignore
    Card_1.getCard(Card_1.SUIT[suitKey], Card_1.CARD_VALUE[cardKey]))));
};
exports.createFreshDeck = createFreshDeck;
const shuffle = (deck) => {
    for (let i = deck.length - 1; i > 0; i--) {
        const newIndex = Math.floor(Math.random() * (i + 1));
        const oldValue = deck[newIndex];
        deck[newIndex] = deck[i];
        deck[i] = oldValue;
    }
    return deck;
};
exports.shuffle = shuffle;
