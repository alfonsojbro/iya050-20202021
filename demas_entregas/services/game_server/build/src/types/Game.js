"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.joinRoom = exports.createNewGame = exports.generateRoomCode = void 0;
const Deck_1 = require("./Deck");
const Turn_1 = require("./Turn");
const generateRoomCode = () => {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
};
exports.generateRoomCode = generateRoomCode;
const createNewGame = ({ playerId, wins, loses, }) => {
    if (!playerId)
        return null;
    const deckCards = Deck_1.createFreshDeck();
    const roomId = exports.generateRoomCode();
    const deckSplitPoint = deckCards.length - 9;
    const mainDeck = { cards: deckCards.slice(0, deckSplitPoint) };
    const playerDeck = {
        cards: deckCards.slice(deckSplitPoint, deckCards.length),
    };
    const players = [];
    const playerCreator = {
        playerId,
        wins,
        loses,
        deck: playerDeck,
        melds: [],
    };
    players.push(playerCreator);
    const game = {
        roomId,
        turn: {
            holderPlayerId: playerId,
            type: Turn_1.TURN_TYPE.DRAW,
            playerId,
        },
        players,
        createdAt: new Date().toDateString(),
        deck: mainDeck,
        waste: null,
    };
    return game;
};
exports.createNewGame = createNewGame;
const joinRoom = ({ roomId, playerId, wins, loses, }) => {
    if (!roomId || !playerId)
        return null;
    //TODO: Get the room from the persistance service by roomId
    //Creating a dummy game for now
    const game = exports.createNewGame({ playerId, wins: 20, loses: 30 });
    const deckSplitPoint = game.deck.cards.length - 9;
    const playerDeck = {
        cards: game.deck.cards.slice(deckSplitPoint, game.deck.cards.length),
    };
    const mainDeck = { cards: game.deck.cards.slice(0, deckSplitPoint) };
    const player = {
        playerId,
        wins,
        loses,
        deck: playerDeck,
        melds: [],
    };
    game.players.push(player);
    game.deck = mainDeck;
    return game;
};
exports.joinRoom = joinRoom;
