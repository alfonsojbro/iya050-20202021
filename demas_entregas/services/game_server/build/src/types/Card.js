"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CARD_VALUE = exports.SUIT = exports.getCard = void 0;
const getCard = (suit, value) => {
    return {
        suit,
        value,
    };
};
exports.getCard = getCard;
var SUIT;
(function (SUIT) {
    SUIT["EPEE"] = "\u2660";
    SUIT["TREFLE"] = "\u2663";
    SUIT["COEUR"] = "\u2665";
    SUIT["DIAMANT"] = "\u2666";
})(SUIT = exports.SUIT || (exports.SUIT = {}));
var CARD_VALUE;
(function (CARD_VALUE) {
    CARD_VALUE["A"] = "A";
    CARD_VALUE["DEUX"] = "2";
    CARD_VALUE["TROIS"] = "3";
    CARD_VALUE["QUATRE"] = "4";
    CARD_VALUE["CINQ"] = "5";
    CARD_VALUE["SIX"] = "6";
    CARD_VALUE["SEPT"] = "7";
    CARD_VALUE["HUIT"] = "8";
    CARD_VALUE["NEUF"] = "9";
    CARD_VALUE["DIX"] = "10";
    CARD_VALUE["ONZE"] = "J";
    CARD_VALUE["DOUZE"] = "Q";
    CARD_VALUE["TREIZE"] = "K";
})(CARD_VALUE = exports.CARD_VALUE || (exports.CARD_VALUE = {}));
