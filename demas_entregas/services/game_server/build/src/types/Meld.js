"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateGameWMeld = exports.isValid = void 0;
function isValid(cards) {
    if (cards.length < 3)
        return false;
    const cardsSelectedSorted = cards.sort((a, b) => {
        const aValue = convertFromStringToNumberValue(a.value);
        const bValue = convertFromStringToNumberValue(b.value);
        return parseInt(aValue) - parseInt(bValue);
    });
    if (isStraight(cardsSelectedSorted) || isSameNumber(cardsSelectedSorted)) {
        return true;
    }
    return false;
}
exports.isValid = isValid;
function isStraight(cards) {
    let previousCard = cards[0];
    for (let x = 1; x < cards.length; x++) {
        const value = (parseInt(previousCard.value, 10) + 1).toString();
        const suit = previousCard.suit;
        if (value != cards[x].value || suit != cards[x].suit) {
            return false;
        }
        previousCard = cards[x];
    }
    return true;
}
function isSameNumber(cards) {
    let value = cards[0].value;
    for (let x = 0; x < cards.length; x++) {
        if (cards[x].value !== value) {
            return false;
        }
    }
    return true;
}
function convertFromStringToNumberValue(value) {
    if (value === "J")
        value = "11";
    else if (value === "Q")
        value = "12";
    else if (value === "K")
        value = "13";
    else if (value === "A")
        value = "1";
    return value;
}
function updateGameWMeld(game, player, meld) {
    //Update player melds with new meld
    if (!player.melds)
        player.melds = [];
    player.melds.push(meld);
    const playerIndex = game.players.findIndex((gamePlayer) => gamePlayer.playerId === player.playerId);
    game.players[playerIndex] = player;
    return game;
}
exports.updateGameWMeld = updateGameWMeld;
