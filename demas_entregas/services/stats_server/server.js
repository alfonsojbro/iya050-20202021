/*/
1. El servidor debe ser capaz de gestionar datos sobre partidas de un determinado videojuego. Estos datos deben contener, al menos, el [estado](<https://en.wikipedia.org/wiki/State_(computer_science)#Program_state>) de la partida.
1. El servidor debe permitir obtener datos de las partidas de un jugador concreto.
1. El servidor debe permitir obtener datos estadísticos sobre el videojuego.
1. El servidor debe permitir almacenar los datos anteriores a través de [mutaciones](https://graphql.org/learn/queries/#mutations).
1. El servidor debe utilizar el siguiente servicio web dado por el profesor para persistir la información necesaria: [endpoint](https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development), [documentación](https://github.com/gmunguia/persistence-service/).
*/
import { GraphQLServer } from "graphql-yoga";
import fetch from "node-fetch";

// `fetch` es una API de navegador. Por lo tanto, no está disponible en NodeJS. El paquete `node-fetch` implementa un clon de `fetch` utilizable en NodeJS.

const applicationId = "8e396b2f-76df-213n-9448-3bae6a4a22ff";
const BASE_URL =
  "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";

const BASE_HEADERS = {
  "x-application-id": applicationId,
};

const fetchJson = (...args) =>
  fetch(...args).then((response) => response.json());

const schema = `
type Query {
    info: String!
    getPlayerGames(playerId: String!): [Game]!
    getGlobalStats: GlobalStats
    getGame(id: String!): Game
   
}
type Mutation {
    updateGlobalStats(score: Int): GlobalStats!
    updatePlayerInfo(name: String!, username: String!): Player!
    setActiveGame(id: String): Boolean!
    createGame(roomId: String, playerId: String): Game!
    joinRoom(roomId: String, playerId: String): Game!
    saveGame(id: String, game: GameInput): Game!
}

type GlobalStats {
    gamesPlayed: Int!
    highestScore: Int!
}
type Player {
    playerId: String
    wins: Int
    loses: Int
    deck: Deck
    melds: [Meld]
}
  type Meld {
    cards: [Card]
  }
  type Deck {
    cards: [Card]
  }
  type Card {
    value: String
    suit: String
  }

  type Game {
    roomId: String
    createdAt: String
    players: [Player]
    waste: Card
    deck: Deck
    turn: Turn
  }

   type Turn  {
    playerId: String
    type: String
    holderPlayerId: String
  }

  input GameInput {
    roomId: String
    createdAt: String
    players: [PlayerInput]
    waste: CardInput
    deck: DeckInput
    turn: TurnInput
  }
  
  input TurnInput  {
    playerId: String
    type: String
    holderPlayerId: String
  }
  input PlayerInput {
    playerId: String
    wins: Int
    loses: Int
    deck: DeckInput
    melds: [MeldInput]
}
input MeldInput {
  cards: [CardInput]
}
input DeckInput {
  cards: [CardInput]
}
input CardInput {
  value: String
  suit: String
}
  `;

const resolvers = {
  Query: {
    getPlayerGames: (_, { playerId }) =>
      fetchJson(`${BASE_URL}/pairs/${playerId}`, {
        method: "GET",
        headers: { ...BASE_HEADERS },
      }).then((result) => {
        if (Array.isArray(result) && result.length != 0) {
          return result.map((obj) => {
            const value = JSON.parse(obj.value);

            if (value.playerId && value.playerId === playerId) {
              console.log(value);
              return value;
            }
          });
        }
        return [];
      }),
    getGame: (_, { id }) =>
      fetchJson(`${BASE_URL}/pairs/${id}/`, {
        method: "GET",
        headers: { ...BASE_HEADERS },
      }).then(({ value }) => {
        console.log(value);
        const { game } = JSON.parse(value);
        return game;
      }),

    getGlobalStats: (_, __) =>
      fetchJson(`${BASE_URL}/pairs/globalStats`, {
        method: "GET",
        headers: { ...BASE_HEADERS },
      }),
  },
  Mutation: {
    createGame: (_, { roomId, playerId }) => {
      const game = createNewGame({ roomId, playerId });

      return fetchJson(`${BASE_URL}/pairs/${roomId}/`, {
        method: "PUT",
        headers: { ...BASE_HEADERS },
        body: JSON.stringify({
          game,
        }),
      }).then((result) => {
        return game;
      });
    },
    joinRoom: (_, { roomId, playerId }) => {
      return fetchJson(`${BASE_URL}/pairs/${roomId}/`, {
        method: "GET",
        headers: { ...BASE_HEADERS },
      }).then(({ value }) => {
        const { game } = JSON.parse(value);

        const gameJoined = joinRoom({ game, playerId });
        return fetchJson(`${BASE_URL}/pairs/${roomId}/`, {
          method: "PUT",
          headers: { ...BASE_HEADERS },
          body: JSON.stringify({
            game: gameJoined,
          }),
        }).then(({ value }) => {
          console.log(gameJoined);

          return gameJoined;
        });
      });
    },
    saveGame: (_, { id, game }) => {
      return fetchJson(`${BASE_URL}/pairs/${id}/`, {
        method: "PUT",
        headers: { ...BASE_HEADERS },

        body: JSON.stringify({ game }),
      }).then((result) => {
        console.log(game);

        return game;
      });
    },
  },
};

const server = new GraphQLServer({
  typeDefs: schema,
  resolvers,
});

const options = {
  port: process.env.PORT || 4000,
  function() {
    console.log("SERVER STARTED PORT: 3000");
  },
};
server.start(options, ({ port }) =>
  console.log(
    `Server started, listening on port ${port} for incoming requests.`
  )
);

const createNewGame = ({ roomId, playerId }) => {
  if (!playerId) return null;
  const deckCards = createFreshDeck();

  const deckSplitPoint = deckCards.length - 9;

  const mainDeck = { cards: deckCards.slice(0, deckSplitPoint) };
  const playerDeck = {
    cards: deckCards.slice(deckSplitPoint, deckCards.length),
  };
  const players = [];
  const playerCreator = {
    playerId,

    deck: playerDeck,
    melds: { cards: [] },
  };
  players.push(playerCreator);
  const waste = mainDeck.cards.pop();
  const game = {
    roomId,
    turn: {
      holderPlayerId: playerId,
      type: TURN_TYPE.DRAW,
      playerId,
    },
    players,
    createdAt: new Date().toDateString(),
    deck: mainDeck,
    waste,
  };
  return game;
};
const createFreshDeck = () => {
  return shuffle(
    Object.keys(SUIT).flatMap((suitKey) =>
      Object.keys(CARD_VALUE).map((cardKey) =>
        getCard(SUIT[suitKey], CARD_VALUE[cardKey])
      )
    )
  );
};

export const getCard = (suit, value) => {
  return {
    suit,
    value,
  };
};

const SUIT = {
  EPEE: "♠",
  TREFLE: "♣",
  COEUR: "♥",
  DIAMANT: "♦",
};
const CARD_VALUE = {
  A: "A",
  DEUX: "2",
  TROIS: "3",
  QUATRE: "4",
  CINQ: "5",
  SIX: "6",
  SEPT: "7",
  HUIT: "8",
  NEUF: "9",
  DIX: "10",
  ONZE: "J",
  DOUZE: "Q",
  TREIZE: "K",
};

const TURN_TYPE = {
  DRAW: "DRAW",
  DROP: "DROP",
  PICK: "PICK",
};

const joinRoom = ({ game, playerId }) => {
  if (!game || !playerId) return null;

  const deckSplitPoint = game.deck.cards.length - 9;
  const playerDeck = {
    cards: game.deck.cards.slice(deckSplitPoint, game.deck.cards.length),
  };
  const mainDeck = { cards: game.deck.cards.slice(0, deckSplitPoint) };

  const player = {
    playerId,

    deck: playerDeck,
    melds: [],
  };
  game.players.push(player);
  game.deck = mainDeck;
  return game;
};
const shuffle = (deck) => {
  for (let i = deck.length - 1; i > 0; i--) {
    const newIndex = Math.floor(Math.random() * (i + 1));
    const oldValue = deck[newIndex];
    deck[newIndex] = deck[i];
    deck[i] = oldValue;
  }
  return deck;
};
