
export const createNewGame = ({ roomId,playerId }) => {
    if (!playerId) return null;
    const deckCards = createFreshDeck();
    const roomId = generateRoomCode();
    const deckSplitPoint = deckCards.length - 9;
  
    const mainDeck = { cards: deckCards.slice(0, deckSplitPoint) };
    const playerDeck = {
      cards: deckCards.slice(deckSplitPoint, deckCards.length),
    };
    const players = [];
    const playerCreator = {
      playerId,
      wins,
      loses,
      deck: playerDeck,
      melds: [],
    };
    players.push(playerCreator);
  
    const game = {
      roomId,
      turn: {
        holderPlayerId: playerId,
        type: TURN_TYPE.DRAW,
        playerId,
      },
      players,
      createdAt: new Date().toDateString(),
      deck: mainDeck,
      waste: null,
    };
    return game;
  };
 export  const createFreshDeck = () => {
   
    return shuffle(
     
      Object.keys(SUIT).flatMap((suitKey) =>
        Object.keys(CARD_VALUE).map((cardKey) =>
         
          getCard(SUIT[suitKey], CARD_VALUE[cardKey])
        )
      )
    );
  };
  

  
  export const SUIT  = {
    EPEE = "♠",
    TREFLE = "♣",
    COEUR = "♥",
    DIAMANT = "♦",
  }
 export  const CARD_VALUE =  {
    A = "A",
    DEUX = "2",
    TROIS = "3",
    QUATRE = "4",
    CINQ = "5",
    SIX = "6",
    SEPT = "7",
    HUIT = "8",
    NEUF = "9",
    DIX = "10",
    ONZE = "J",
    DOUZE = "Q",
    TREIZE = "K",
  }
  export const joinRoom = ({
   game,
    playerId,
   
  }) => {
    if (!roomId || !playerId) return null;

    const deckSplitPoint = game.deck.cards.length - 9;
    const playerDeck = {
      cards: game.deck.cards.slice(deckSplitPoint, game.deck.cards.length),
    };
    const mainDeck = { cards: game.deck.cards.slice(0, deckSplitPoint) };
  
    const player = {
      playerId,
      wins,
      loses,
      deck: playerDeck,
      melds: [],
    };
    game.players.push(player);
    game.deck = mainDeck;
    return game;
  };
  