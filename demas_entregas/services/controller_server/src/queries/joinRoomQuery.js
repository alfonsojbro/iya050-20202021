module.exports = `mutation joinRoom($roomId: String, $playerId: String) {
    joinRoom(roomId: $roomId, playerId: $playerId){
      roomId,
      createdAt,
      turn {
        playerId
        type,
        holderPlayerId
    	},
      waste{
        value,
        suit
      },

  
      players{
        playerId,
        deck{
          cards{
            value,
            suit
          }
        },
        melds{
          cards{
            value,
            suit
          }
        }

      },
      deck{
        cards{
          value,
          suit
        }
      },

    
  	}
  }`;
