module.exports = `mutation saveGame($id: String, $game: GameInput) {
  saveGame(id: $id, game: $game){
    roomId,
    createdAt,
    turn {
      playerId
      type,
      holderPlayerId
    },
    waste{
      value,
      suit
    },


    players{
      playerId,
      deck{
        cards{
          value,
          suit
        }
      },
      melds{
        cards{
          value,
          suit
        }
      }

    },
    deck{
      cards{
        value,
        suit
      }
    },

  
  }
}`;
