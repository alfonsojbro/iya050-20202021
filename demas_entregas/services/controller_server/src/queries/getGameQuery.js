module.exports = `query getGame($id: String!) {
    getGame(id: $id){
     	roomId,
      createdAt,
      turn {
        playerId
        type,
        holderPlayerId
    	},
      waste{
        value,
        suit
      },

  
      players{
        playerId,
        deck{
          cards{
            value,
            suit
          }
        },
        melds{
          cards{
            value,
            suit
          }
        }

      },
      deck{
        cards{
          value,
          suit
        }
      },

    
  	}
}
`;
