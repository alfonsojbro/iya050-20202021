module.exports = `mutation createGame($roomId: String, $playerId: String) {
    createGame(roomId: $roomId, playerId: $playerId){
      roomId,
      createdAt,
      turn {
        playerId
        type,
        holderPlayerId
    	},
      waste{
        value,
        suit
      },

  
      players{
        playerId,
        deck{
          cards{
            value,
            suit
          }
        },
        melds{
          cards{
            value,
            suit
          }
        }

      },
      deck{
        cards{
          value,
          suit
        }
      },

    
  	}
  }`;
