const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");
const fetch = require("node-fetch");
const saveGameQuery = require("./queries/saveGameQuery");
const getGameQuery = require("./queries/getGameQuery");
const createGameQuery = require("./queries/createGameQuery");
const joinRoomQuery = require("./queries/joinRoomQuery");
const app = new Koa();
const router = new Router();
//const gameURL = "https://enigmatic-ravine-16547.herokuapp.com/";
const gameURL = "http://localhost:3000";
const serverURL = "http://localhost:4000";
app.use(async (ctx, next) => {
  console.log("received request", ctx.path);
  await next();
});

router.get("/getGame/:roomId", (ctx) => {
  console.log(ctx.params.roomId);
  return fetch(serverURL, {
    headers: { "Content-Type": "application/json" },
    method: "POST",
    body: JSON.stringify({
      query: getGameQuery,
      variables: { id: ctx.params.roomId },
    }),
  })
    .then((res) => res.json())
    .then(({ data }) => {
      const { getGame } = data;

      console.log(getGame);
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        game: getGame,
      });
    });
});
router.get("/game/createGame/:roomId/:playerId", (ctx) => {
  // 1. post mutation to Stats server to store new game with new Player Id
  // 2. return state

  return fetch(serverURL, {
    headers: { "Content-Type": "application/json" },
    method: "POST",
    body: JSON.stringify({
      query: createGameQuery,
      variables: { roomId: ctx.params.roomId, playerId: ctx.params.playerId },
    }),
  })
    .then((res) => res.json())
    .then(({ data }) => {
      const { createGame } = data;

      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        game: createGame,
      });
    })
    .catch((error) => console.log(error));
});

router.get("/joinRoom/:roomId/:playerId", (ctx) => {
  // 1. post new player to the game
  // 2. return state

  return fetch(serverURL, {
    headers: { "Content-Type": "application/json" },
    method: "POST",
    body: JSON.stringify({
      query: joinRoomQuery,
      variables: { roomId: ctx.params.roomId, playerId: ctx.params.playerId },
    }),
  })
    .then((res) => res.json())
    .then(({ data }) => {
      const game = data.joinRoom;
      console.log(game);
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        game,
      });
    });
});

router.post("/makeMeld/:id", (ctx) => {
  // 1. get current state from Stats server
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4.    console.log("Meld", ctx.request.body);return state

  const game = ctx.request.body.game;
  const meld = ctx.request.body.meld;
  console.log("Req", game, meld);
  return fetch(gameURL + "/makeMeld", {
    headers: { "Content-Type": "application/json" },
    method: "POST",
    body: JSON.stringify({ game, meld }),
  })
    .then((res) => res.json())
    .then((gameMelded) => {
      return fetch(serverURL, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          query: saveGameQuery,
          variables: { id: ctx.params.roomId, game: gameMelded.game },
        }),
      })
        .then((res) => res.json())
        .then(({ data }) => {
          console.log("GameMelded", data.saveGame.players[0]);
          ctx.response.set("Content-Type", "application/json");
          ctx.body = JSON.stringify({
            game,
          });
        });
    });
});

router.post("/dropCard/:id", (ctx) => {
  // 1. get current state from Stats server
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4. return state

  return fetch(serverURL, {
    headers: { "Content-Type": "application/json" },
    method: "POST",
    body: JSON.stringify({
      query: getGameQuery,
      variables: { id: ctx.params.id },
    }),
  })
    .then((res) => res.json())
    .then(({ data }) => {
      return fetch(gameURL + "/dropCard", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          game: data.getGame,
          card: ctx.request.body.card,
        }),
      })
        .then((res) => res.json())
        .then(({ game }) => {
          console.log("DropCardRes", game);
          return fetch(serverURL, {
            headers: { "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify({
              query: saveGameQuery,
              variables: { id: game.roomId, game },
            }),
          })
            .then((res) => res.json())
            .then(({ data }) => {
              ctx.response.set("Content-Type", "application/json");
              ctx.body = JSON.stringify({
                game,
              });
            });
        });
    });
});

router.get("/drawCard/:id", (ctx) => {
  // 1. get current state from Stats server
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4. return state

  return fetch(serverURL, {
    headers: { "Content-Type": "application/json" },
    method: "POST",
    body: JSON.stringify({
      query: getGameQuery,
      variables: { id: ctx.params.id },
    }),
  })
    .then((res) => res.json())
    .then(({ data }) => {
      const { getGame } = data;

      const game = getGame;

      console.log("GameData", data);
      return fetch(gameURL + "/drawCard", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ game }),
      })
        .then((res) => res.json())
        .then(({ game }) => {
          console.log("DrawCard", game);
          return fetch(serverURL, {
            headers: { "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify({
              query: saveGameQuery,
              variables: { id: game.roomId, game },
            }),
          })
            .then((res) => res.json())
            .then(({ data }) => {
              ctx.response.set("Content-Type", "application/json");
              ctx.body = JSON.stringify({
                game,
              });
            });
        });
    });
});

router.get("/passTurn/:id", (ctx) => {
  // 1. get current state from Stats server
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4. return state

  return fetch(serverURL, {
    headers: { "Content-Type": "application/json" },
    method: "POST",
    body: JSON.stringify({
      query: getGameQuery,
      variables: { id: ctx.params.id },
    }),
  })
    .then((res) => res.json())
    .then(({ data }) => {
      const { getGame } = data;

      const game = getGame;

      return fetch(gameURL + "/passTurn", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ game }),
      })
        .then((res) => res.json())
        .then(({ game }) => {
          return fetch(serverURL, {
            headers: { "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify({
              query: saveGameQuery,
              variables: { id: game.roomId, game },
            }),
          })
            .then((res) => res.json())
            .then(({ data }) => {
              console.log("GameResponse", game);
              ctx.response.set("Content-Type", "application/json");
              ctx.body = JSON.stringify({
                game,
              });
            });
        });
    });
});
app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT);
