import React, { useContext, useEffect, useState } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import AuthContext from "../AuthContext.js";
import getPlayerGames from "../database/getPlayerGames";
import saveGame from "../database/saveGame";
const History = () => {
  const history = useHistory();
  const { user } = useContext(AuthContext);

  const [games, setGames] = useState([]);

  useEffect(() => {
    getPlayerGames(user.id).then((result) => {
      setGames(result.data.getPlayerGames);
    });
  }, []);

  const watchGame = (game) => {
    saveGame({ ...game, isActive: true });
    history.push({ pathname: `play`, state: { game } });
  };
  return (
    <div>
      <p>{user.name}’s past games</p>
      <table>
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Puntuación</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          {games.map((game) => {
            if (game)
              return (
                <tr key={game.id}>
                  <td>{game.createdAt}</td>
                  <td>{game.score}</td>
                  <td>{game.isActive ? "Activa" : "Inactiva"}</td>
                  {!game.isActive ? (
                    <td>
                      <button onClick={() => watchGame(game)}>
                        Ver partida
                      </button>
                    </td>
                  ) : null}
                </tr>
              );
          })}
        </tbody>
      </table>
    </div>
  );
};
/*   */

export default History;
