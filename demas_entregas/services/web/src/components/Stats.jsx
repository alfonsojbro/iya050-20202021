import { number } from "prop-types";
import React, { useContext, useState, useEffect } from "react";
import AuthContext from "../AuthContext.js";
import getPlayerGames from "../database/getPlayerGames";
const Stats = () => {
  const { user } = useContext(AuthContext);

  const [globalStats, setGlobalStats] = useState({
    highestScore: 0,
    gamesPlayed: 0,
  });

  useEffect(() => {
    getPlayerGames(user.id).then((result) => {
      const games = result.data.getPlayerGames;

      let maxScore = 0;
      let numberOfGames = 0;
      games.map((game) => {
        if (game && game.score) {
          if (game.score > maxScore) maxScore = game.score;

          numberOfGames++;
        }
      });
      setGlobalStats({ highestScore: maxScore, gamesPlayed: numberOfGames });
      console.log(numberOfGames);
      console.log(maxScore);
    });
  }, []);

  return (
    <div>
      {" "}
      <p>Game stats</p>
      <div>
        <table>
          <thead>
            <tr>
              <th>Highest Scoore</th>
              <th>Number of games </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{globalStats.highestScore}</td>
              <td>{globalStats.gamesPlayed}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

const getGlobalStats = () => {
  return { highestScore: 0, gamesPlayed: 0 };
};

export default Stats;
