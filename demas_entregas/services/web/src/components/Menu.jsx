import React, { useContext, usen, useState } from "react";
import {
  Link,
  Redirect,
  Route,
  Switch,
  useRouteMatch,
  useHistory,
} from "react-router-dom";
import AuthContext from "../AuthContext.js";
import Stats from "./Stats.jsx";
import Play from "./Play.jsx";
import History from "./History.jsx";

const Menu = (props) => {
  const { isLoggedIn, user } = useContext(AuthContext);
  const { path, url } = useRouteMatch();
  let history = useHistory();
  if (!isLoggedIn) return <Redirect to="/sign-in" />;
  const [roomCode, setRoomCode] = useState(null);
  return (
    <>
      <nav style={{ backgroundColor: "#333", overflow: "hidden" }}>
        <Link to={`${url}/stats`} style={style.listItem}>
          Stats
        </Link>

        <button
          onClick={() => {
            const newId = generateRoomCode();
            history.push({
              pathname: `${url}/${newId}`,
            });
          }}
          style={style.menuButton}
        >
          New game
        </button>

        <Link style={style.listItem} to={`${url}/history`}>
          Past games
        </Link>
      </nav>
      <Switch>
        <Route exact path="/game">
          <div>
            <h2>Welcome, {user.name}</h2>
            <h4>Join Room</h4>
            <input
              value={roomCode}
              onChange={(e) => setRoomCode(e.target.value)}
            ></input>

            <button
              onClick={(e) =>
                history.push({
                  pathname: `${url}/${roomCode}`,
                  state: {
                    joinRoom: true,
                  },
                })
              }
            >
              Unirse a sala
            </button>
          </div>
        </Route>

        <Route path={`${path}/stats`}>
          <Stats />
        </Route>

        <Route
          path={`${path}/:id`}
          render={(props) => <Play props={props} />}
        ></Route>

        <Route path={`${path}/history`}>
          <History />
        </Route>
      </Switch>
    </>
  );
};
const style = {
  listItem: {
    float: "left",
    color: "#f2f2f2",
    textAlign: "center",
    padding: "14px 16px",

    textDecoration: "none",
    fontSize: "17px",
  },
  menuButton: {
    float: "left",
    color: "white",
    backgroundColor: "#4CAF50",
    textAlign: "center",
    padding: "14px 16px",

    textDecoration: "none",
    fontSize: "17px",
  },
};
export const generateRoomCode = () => {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};
export default Menu;
