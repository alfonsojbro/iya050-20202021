import React, { useState, useContext, useEffect } from "react";
import AuthContext from "../AuthContext.js";
import CONSTANTS from "../CONSTANTS";
const Play = ({ props }) => {
  const { user } = useContext(AuthContext);
  const [data, setData] = useState(undefined);
  const [cardsSelected, setCardsSelected] = useState([]);
  const state = props.location.state;
  console.log(data);
  useEffect(() => {
    if (state) {
      fetch(
        `${CONSTANTS.CONTROLLER_SERVER_URL}/joinRoom/${CONSTANTS.TEST_ROOM_ID}/${user.id}`
      )
        .then((res) => res.json())
        .then((data) => setData(data.game));
    } else {
      fetch(
        `${CONSTANTS.CONTROLLER_SERVER_URL}/game/createGame/${CONSTANTS.TEST_ROOM_ID}/${user.id}`
      )
        .then((res) => res.json())
        .then((data) => setData(data.game));
    }
  }, []);

  useEffect(() => {
    const handle = setInterval(() => {
      return fetch(
        `${CONSTANTS.CONTROLLER_SERVER_URL}/getGame/${CONSTANTS.TEST_ROOM_ID}`
      )
        .then((res) => res.json())
        .then((data) => setData(data.game));
    }, 5000);

    return () => clearInterval(handle);
  }, []);

  const drawCard = () => {
    fetch(
      `${CONSTANTS.CONTROLLER_SERVER_URL}/drawCard/${CONSTANTS.TEST_ROOM_ID}`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setData(data.game);
      });
  };

  const passTurnToNextPlayer = () => {
    fetch(
      `${CONSTANTS.CONTROLLER_SERVER_URL}/passTurn/${CONSTANTS.TEST_ROOM_ID}`
    )
      .then((res) => res.json())
      .then((data) => {
        setData(data.game);
        setCardsSelected([]);
      });
  };
  const pickCard = () => {
    fetch(
      `${CONSTANTS.CONTROLLER_SERVER_URL}/makeMeld/${CONSTANTS.TEST_ROOM_ID}`,
      {
        headers: { "Content-Type": "application/json" },
        method: "POST",
        body: JSON.stringify({
          meld: { cards: cardsSelected },
          game: data,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setData(data.game);
        setCardsSelected([]);
      });
  };
  const dropCard = () => {
    if (cardsSelected.length <= 0) return;

    fetch(
      `${CONSTANTS.CONTROLLER_SERVER_URL}/dropCard/${CONSTANTS.TEST_ROOM_ID}`,
      {
        headers: { "Content-Type": "application/json" },
        method: "POST",
        body: JSON.stringify({
          card: cardsSelected[0],
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setData(data.game);
        setCardsSelected([]);
      });
  };
  if (!data) return <h1>Cargando...</h1>;
  let buttonText = "Espera tu turno";
  let onClickEvent = () => {};
  if (data.turn.playerId === user.id) {
    if (data.turn.type === "DRAW") {
      onClickEvent = drawCard;
      buttonText = "Saca una carta";
    } else if (data.turn.type === "DROP") {
      onClickEvent = dropCard;
      buttonText = "Bota una carta";
    } else {
      onClickEvent = pickCard;
      buttonText = "Elige una combinación de cartas";
    }
  }

  const isWasteSelected = cardsSelected.find(
    (_card) =>
      data.waste.value === _card.value && data.waste.suit === _card.suit
  );

  return (
    <div style={{ flex: 1, display: "flex", flexDirection: "column" }}>
      <h1>Código de la sala: {data.roomId}</h1>

      <button
        style={{
          width: 50,
          height: 80,
          border: 1,
          borderWidth: 5,
          borderColor: isWasteSelected ? "green" : "blue",
          borderStyle: "dotted",
        }}
        disabled={data.turn.playerId !== user.id}
        onClick={() => {
          setCardsSelected([...cardsSelected, card]);
        }}
      >
        <div>
          <div>{data.waste.suit}</div>
          <div>{data.waste.value}</div>
        </div>
      </button>
      <button
        style={{
          width: 50,
          height: 80,
          border: 1,
          borderWidth: 5,
          borderColor: "blue",
          borderStyle: "dotted",
        }}
      >
        <div>
          <div>{data.deck.cards.length}</div>
        </div>
      </button>

      <h1>Jugadores: </h1>

      <div
        style={{
          flex: 1,
          flexDirection: "row",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        {data.players.map((player, index) => (
          <div
            syle={{ flex: 1, flexDirection: "column", margin: "5%" }}
            key={player.playerId}
          >
            <div>
              <div> #{index + 1}</div>
              <h2>{player.playerId.substring(0, 3)}</h2>
              {data.turn.playerId === player.playerId && <h5>Turno actual</h5>}
            </div>
            <div>
              {player.deck.cards.map((card, index) => {
                const isCardSelected = cardsSelected.find(
                  (_card) =>
                    card.value === _card.value && card.suit === _card.suit
                );
                return (
                  <button
                    style={{
                      border: 1,
                      borderWidth: 5,
                      borderColor: isCardSelected ? "green" : "blue",
                      borderStyle: "dotted",
                    }}
                    disabled={data.turn.playerId !== player.playerId}
                    onClick={() => {
                      setCardsSelected([...cardsSelected, card]);
                    }}
                  >
                    <div>{card.suit}</div>
                    <div>{card.value}</div>
                  </button>
                );
              })}
            </div>
            <div>
              {player.melds &&
                player.melds.map((meld) =>
                  meld.cards.map((cardMeld) => (
                    <button
                      style={{
                        border: 1,
                        borderWidth: 5,
                        borderColor: "yellow",
                        borderStyle: "dotted",
                      }}
                    >
                      <div>{cardMeld.suit}</div>
                      <div>{cardMeld.value}</div>
                    </button>
                  ))
                )}
            </div>
          </div>
        ))}
      </div>

      <button style={{ width: 200 }} onClick={onClickEvent}>
        {buttonText}
      </button>
      {data.turn.type === "PICK" && data.turn.playerId === user.id && (
        <button style={{ width: 200 }} onClick={() => passTurnToNextPlayer()}>
          Pasar turno
        </button>
      )}
    </div>
  );
};

export default Play;
