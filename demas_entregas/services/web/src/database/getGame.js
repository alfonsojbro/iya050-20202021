import CONSTANT from "./CONSTANT";
const query = `query GetGame($id: String!) {
    getGame(id: $id){
        score
        isActive
        createdAt
        playerId
    }
  }`;
const getGame = (id) => {
  return fetch(`${CONSTANT.BASE_URL}`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query,
      variables: { id },
    }),
  })
    .then((res) => res.json())
    .then((res) => console.log(res.data));
};

export default getGame;
