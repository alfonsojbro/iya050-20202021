import CONSTANT from "./CONSTANT";

const query = `mutation SaveGame($id: String!, $createdAt: String!, $playerId: String!, $score: Int!, $isActive: Boolean!) {
    saveGame(id: $id, createdAt: $createdAt, playerId: $playerId, score: $score, isActive: $isActive){
      id, 
      createdAt
    }
  }`;
const SaveGame = ({ id, createdAt, playerId, score, isActive }) => {
  return fetch(`${CONSTANT.BASE_URL}`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query,
      variables: { id, createdAt, playerId, score, isActive },
    }),
  })
    .then((res) => res.json())
    .then((res) => console.log(res.data));
};

export default SaveGame;
