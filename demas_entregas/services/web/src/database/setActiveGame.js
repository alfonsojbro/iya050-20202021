import CONSTANT from "./CONSTANT";
const query = `mutation SetActiveGame($id: String!) {
    setActiveGame(id: $id)
  }`;
const setActiveGame = (id) => {
  return fetch(`${CONSTANT.BASE_URL}/game`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query,
      variables: { id },
    }),
  })
    .then((res) => res.json())
    .then((res) => console.log(res.data));
};

export default setActiveGame;
