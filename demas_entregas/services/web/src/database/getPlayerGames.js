import CONSTANT from "./CONSTANT";
const query = `query GetPlayerGames($playerId: String!) {
    getPlayerGames(playerId: $playerId){
        createdAt,
    	 score,
       id,
       playerId,
    	 isActive
    }
  }`;
const getPlayerGames = (id) => {
  return fetch(`${CONSTANT.BASE_URL}`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query,
      variables: { playerId: id },
    }),
  }).then((res) => res.json());
};

export default getPlayerGames;
