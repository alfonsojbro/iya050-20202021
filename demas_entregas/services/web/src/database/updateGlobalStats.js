import CONSTANT from "./CONSTANT";
const query = `mutation UpdateGlobalStats($score: Int!) {
    updateGlobalStats(score: $score){
    highestScore, 
    gamesPlayed
  }
  }`;
const UpdateGlobalStats = (score) => {
  return fetch(`${CONSTANT.BASE_URL}`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query,
      variables: { score },
    }),
  })
    .then((res) => res.json())
    .then((res) => console.log(res.data));
};

export default UpdateGlobalStats;
